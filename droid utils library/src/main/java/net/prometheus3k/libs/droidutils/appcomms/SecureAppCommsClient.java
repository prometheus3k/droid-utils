/*
 * Copyright [2015] [Babu Madhikarmi]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.prometheus3k.libs.droidutils.appcomms;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import net.prometheus3k.libs.droidutils.HashHelper;

import org.apache.commons.lang3.RandomStringUtils;

/**
 *
 * Ref: https://en.wikipedia.org/wiki/Message_authentication_code
 * Created by babumadhikarmi on 08/07/2015.
 */
public class SecureAppCommsClient extends AppCommsClient{
    private static final String TAG = SecureAppCommsClient.class.getSimpleName();

    public enum Result {
        PASS,
        FAIL,
        ERROR
    }

    public interface AuthResultHandler{
        void onResult(Result result);
    }

    public SecureAppCommsClient(Context context, String serverPackage, String serverClassName) {
        super(context, serverPackage, serverClassName);
    }

    public void sendAuthChallenge(final String SALT, final AuthResultHandler authResultHandler) {
        //generate random string
        String seed = RandomStringUtils.randomAlphabetic(24);
        final byte[] localHash = HashHelper.create(seed, SALT.getBytes());

        Bundle data = new Bundle();
        data.putString(AppCommsConstants.KEY_SEED, seed);

        super.sendMessage(data, new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                //if hash match, return true else return false
                byte[] remoteHash = message.getData().getByteArray(AppCommsConstants.KEY_SERVER_HASH);
                if(remoteHash == null || remoteHash.length == 0){
                    Log.e(TAG, "NO HASH RECEIVED!!");
                    authResultHandler.onResult(Result.ERROR);

                } else {
                    boolean match = HashHelper.verify(localHash, remoteHash);
                    Log.e(TAG, "HASH RECEIVED!! match? " + match);
                    authResultHandler.onResult(match ? Result.PASS : Result.FAIL);
                }
                return false;
            }
        });
    }
}
