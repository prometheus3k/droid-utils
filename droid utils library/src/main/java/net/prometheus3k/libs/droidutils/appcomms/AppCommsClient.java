/*
 * Copyright [2015] [Babu Madhikarmi]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.prometheus3k.libs.droidutils.appcomms;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Messenger;
import android.util.Log;

/**
 * Created by babumadhikarmi on 03/07/2015.
 */
public class AppCommsClient{
    private static final String TAG = AppCommsClient.class.getSimpleName();
    private Context mContext;

    private final String mServerPackage;
    private final String mServerClassName;//The IntentService

    public AppCommsClient(Context context, String serverPackage, String serverClassName){
        mContext = context;
        mServerPackage = serverPackage;
        mServerClassName = serverClassName;
    }

    public void sendMessage(Bundle data, Handler.Callback callback){
        //Create a MessageReceivedHandler
        Messenger messenger = new Messenger(new Handler(callback));
        Intent intent = new Intent();
        //Send messenger through to server
        intent.putExtra(AppCommsConstants.KEY_MESSENGER, messenger);
        intent.putExtra(AppCommsConstants.KEY_DATA, data);

        intent.setComponent(new ComponentName(mServerPackage, mServerPackage + "." + mServerClassName));
        Log.w(TAG, "Connecting to " + intent.getComponent().getClassName());
        mContext.startService(intent);
    }
}
