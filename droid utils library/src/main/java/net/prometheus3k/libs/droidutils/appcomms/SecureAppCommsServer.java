/*
 * Copyright [2015] [Babu Madhikarmi]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.prometheus3k.libs.droidutils.appcomms;

import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import net.prometheus3k.libs.droidutils.HashHelper;

/**
 * Created by babumadhikarmi on 08/07/2015.
 */
public class SecureAppCommsServer extends AppCommsServer {

    private static String sSalt;

    public SecureAppCommsServer(final String salt){
        sSalt = salt;
        if(isEmptyOrNull(sSalt)){
            throw new RuntimeException("Salt cannot be empty or null");
        }
    }

    private boolean isEmptyOrNull(String value){
        if(value == null || value.isEmpty()){
            return true;
        }
        return false;
    }

    @Override
    public void onMessageReceived(Bundle data, Messenger outMessenger) {
        //get seed from message
        String seed = data.getString(AppCommsConstants.KEY_SEED);

        if(!isEmptyOrNull(seed)) {
            if(outMessenger != null) {
                //generate our own hash from received seed
                byte[] hash = HashHelper.create(seed, sSalt.getBytes());

                //put in Bundle and send off back to Client as response
                Bundle responseData = new Bundle();
                responseData.putByteArray(AppCommsConstants.KEY_SERVER_HASH, hash);

                try {
                    Message message = new Message();
                    message.setData(responseData);
                    outMessenger.send(message);

                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
