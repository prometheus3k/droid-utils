/*
 * Copyright [2015] [Babu Madhikarmi]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.prometheus3k.libs.droidutils.appcomms;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Messenger;
import android.util.Log;

/**
 * An IntentService that receives an Intent
 * and relays to a Callback so that a message can be
 * read by the implementation of AppCommsServer.Callback
 *
 * Created by babumadhikarmi on 03/07/2015.
 *
 */
public abstract class AppCommsServer extends IntentService{
    private static final String TAG = AppCommsServer.class.getSimpleName();

    public AppCommsServer() {
        super(TAG);
        Log.w(TAG, "new AppCommsServer()");
    }

    //The Intent was targeted to us (via subclass).
    @Override
    protected void onHandleIntent(Intent intent) {
        Log.w(TAG, "onHandleIntent()");
        onMessageReceived(intent.getBundleExtra(AppCommsConstants.KEY_DATA), (Messenger)intent.getParcelableExtra(AppCommsConstants.KEY_MESSENGER));
    }

    public abstract void onMessageReceived(Bundle data, Messenger outMessenger);

}
